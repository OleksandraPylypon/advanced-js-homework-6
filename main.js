const button = document.querySelector(".btn");
const information = document.querySelector(".information-by-ip");

button.addEventListener("click", () => {
  const searchByIP = async () => {
    let response = await fetch("https://api.ipify.org/?format=json");
    let ip = response.json();
    return ip;
  };
  searchByIP().then((ip) => console.log(ip));

  const ipAdress = async () => {
    let response = await fetch(
      `http://ip-api.com/json/77.52.66.78?fields=continent,country,region,regionName,city,query`
    );
    let data = await response.json();
    return data;
  };
  ipAdress().then((data) => informationBy(data));
});

function informationBy(data) {
  const ipElement = document.createElement("p");
  ipElement.textContent = `IP: ${data.query}`;
  const continentElement = document.createElement("p");
  continentElement.textContent = `Континент: ${data.continent}`;
  const countryElement = document.createElement("p");
  countryElement.textContent = `Країна: ${data.country}`;
  const cityElement = document.createElement("p");
  cityElement.textContent = `Місто: ${data.city}`;
  const regionElement = document.createElement("p");
  regionElement.textContent = `Регіон: ${data.region}`;
  const regionNameElement = document.createElement("p");
  regionNameElement.textContent = `Район: ${data.regionName}`;

  information.appendChild(ipElement);
  information.appendChild(continentElement);
  information.appendChild(countryElement);
  information.appendChild(cityElement);
  information.appendChild(regionElement);
  information.appendChild(regionNameElement);
}
